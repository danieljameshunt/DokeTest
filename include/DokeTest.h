#ifndef DokeTest_H
#define DokeTest_H

#include "Analysis.h"

#include "CutsDokeTest.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class DokeTest : public Analysis {

public:
    DokeTest();
    ~DokeTest();

    void Initialize();
    void Execute();
    void Finalize();
    void DQ(char* Cut, char* Set);

    int numSS = 0;
    double S1_area = 0;
    double S2_area = 0;
    double driftTime_us = 0;
    double X = 0;
    double Y = 0;
    double Rsq = 0;

protected:
    CutsDokeTest* m_cutsDokeTest;
    ConfigSvc* m_conf;
};

#endif
