#ifndef CutsDokeTest_H
#define CutsDokeTest_H

#include "EventBase.h"

class CutsDokeTest {

public:
    CutsDokeTest(EventBase* eventBase);
    ~CutsDokeTest();
    bool DokeTestCutsOK();
    bool Fiducial1T(double driftTime, double Rsq);
    bool TBADT();

private:
    EventBase* m_event;
};

#endif
