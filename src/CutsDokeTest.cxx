#include "CutsDokeTest.h"
#include "ConfigSvc.h"

CutsDokeTest::CutsDokeTest(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsDokeTest::~CutsDokeTest()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsDokeTest::DokeTestCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}

bool CutsDokeTest::Fiducial1T(double driftTime, double Rsq){

  return (driftTime < 800) && (driftTime > 100) && (Rsq < 3000);

}

bool CutsDokeTest::TBADT(){

  return true;

}
