#include "DokeTest.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsDokeTest.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "ConfigSvc.h"
#include "TCutG.h"

// Constructor
DokeTest::DokeTest()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("pulsesTPCHG");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("DokeTest Analysis");

    // Setup the analysis specific cuts.
    m_cutsDokeTest = new CutsDokeTest(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
DokeTest::~DokeTest()
{
    delete m_cutsDokeTest;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void DokeTest::Initialize()
{
    INFO("Initializing DokeTest Analysis");
}

// Execute() - Called once per event.
void DokeTest::Execute()
{
 
  //int nPulses = (*m_event->m_tpcHGPulses)->nPulses;
    
  numSS = (*m_event->m_singleScatter)->nSingleScatters;

  S1_area = (*m_event->m_singleScatter)->s1Area_phd;
  S2_area = (*m_event->m_singleScatter)->s2Area_phd;

  driftTime_us = ((*m_event->m_singleScatter)->driftTime_ns)/1000;
  
  X = (*m_event->m_singleScatter)->x_cm;
  Y = (*m_event->m_singleScatter)->y_cm;
  
  Rsq = X*X + Y*Y;

  // Graphic cut around activation peaks
  TCutG* A = new TCutG("A", 7);
  A->SetPoint(0, 423, 278000);
  A->SetPoint(1, 628, 270000);
  A->SetPoint(2, 1000, 144000);
  A->SetPoint(3, 1032, 110000);
  A->SetPoint(4, 800, 117000);
  A->SetPoint(5, 500, 220000);
  A->SetPoint(6, 423, 728000);
  

  // if there is a single scatter in the event then plot the S1 pulse area
  if (numSS > 0) {
    DQ((char*)"All", (char*)"BG");
    if(m_cutsDokeTest->Fiducial1T(driftTime_us, Rsq)){
      DQ((char*)"Fidu", (char*)"BG");
    }
    if(m_cutsDokeTest->Fiducial1T(driftTime_us, Rsq) && A->IsInside(S1_area, S2_area)){
      DQ((char*)"A", (char*)"BG");
    }
  }
}

// Finalize() - Called once after event loop.
void DokeTest::Finalize()
{
    INFO("Finalizing DokeTest Analysis");
}

void DokeTest::DQ(char* Cut, char* Set){

  // Plotting histograms
  m_hists->BookFillHist(Form("%s/%s/S1S2", Set, Cut), 1000, 0, 10000, 1000, 0, 7, S1_area, log10(S2_area));
  m_hists->BookFillHist(Form("%s/%s/S1S2_ROI", Set, Cut), 1000, 0, 4000, 1000, 10000, 1000000, S1_area, S2_area);
  m_hists->BookFillHist(Form("%s/%s/XY", Set, Cut), 1000, 0, 8000, 1000, -1000, 0, Rsq, -driftTime_us);

}
